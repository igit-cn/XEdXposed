package com.elderdrivers.riru.edxp.config;

import android.annotation.SuppressLint;
import android.os.Build;
public class InstallerChooser {

    @SuppressLint("SdCardPath")
    private static final String DATA_DIR_PATH_PREFIX =
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ? "/data/user_de/0/" : "/data/user/0/";

    public static final String LEGACY_INSTALLER_PACKAGE_NAME = "de.robv.android.xposed.installer";

    public static String INSTALLER_PACKAGE_NAME;
    public static String INSTALLER_DATA_BASE_DIR;

    public static void setInstallerPackageName(String packageName) {
        INSTALLER_PACKAGE_NAME = packageName;
        INSTALLER_DATA_BASE_DIR = DATA_DIR_PATH_PREFIX + INSTALLER_PACKAGE_NAME + "/";
    }
}
